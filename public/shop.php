<?php require_once("../resources/config.php"); ?>

<?php include(TEMPLATE_FRONT . DS . 'header.php'); ?>

<!-- Page Content -->
<div class="container">

    <!-- Jumbotron Header -->
    <header class="jumbotron hero-spacer">
        <h1>Witaj w naszym sklepie!</h1>
        <p>Oferujemy najnowszy sprzęt RTV/AGD w niewiarygodnie niskich cenach.
            <b>Zapoznaj się z nasza ofertą</b></p>
        <p><a class="btn btn-primary btn-large">Nasze promocje!</a>
        </p>
    </header>

    <hr>

    <!-- Title -->
    <div class="row">
        <div class="col-lg-12">
            <h3>Najnowsze produkty</h3>
        </div>
    </div>
    <!-- /.row -->

    <!-- Page Features -->
    <div class="row text-center">

        <?php product_shop(); ?>


    </div>
    <!-- /.row -->

    <hr>



</div>
<!-- /.container -->

<?php include(TEMPLATE_FRONT . DS . 'footer.php'); ?>
