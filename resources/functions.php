<?php
/*
 * 1.FUNCKJE POMOCNICZE
 * 2.WYSWIETLENIE PRODUKTOW W SKLEPIE
 * 3.PANEL ADMINISTRACYJNY
 * 4.WYSWIETLANIE PRODUKTOW W PANELU ADMINISTRACYJNYM
 * 5.OBSLUGA SLIDERA
 * 
 */
//***********************************1.FUNKCJE POMOCNICZE***************************************************************************

function last_id(){
    global $connection;
    return mysqli_insert_id($connection);
}//Wyswietlanie ostatniego id dodanego do bazy danych
    
function set_message($msg){
    if(!empty($msg)){
        $_SESSION['message'] = $msg;
    }else{
        $msg = '';
    }
    
}//Ustawienie wiadomosci 

function display_message(){
    if(isset($_SESSION['message'])){
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }
}//Wyswielnenie wiadomosci

function redirect($loc){ //Funckja przenosi do innego pliku
    header("Location: " . $loc);
}//Przekierowanie

function query($sql){ //Zapytania do bazy danych
    global $connection;
    return mysqli_query($connection, $sql);
}//Zapytanie do bazy danych

function confirm($query){ //Sprawdzenie czy zapytanie do bazy danych jest poprawne
    global $connection;
    
    if(!$query){
        die('Blad bazy danych'. mysqli_error($connection));
    }
}//Sprawdzenie zapytan do bazy danych

function escape_string($string){ //Oczyszczanie zapytan SQL
    global $connection;
    return mysqli_real_escape_string($connection, $string);
}//Oczyszczenie danych wprowadzanych do bazy danych

function fetch_array($row){ //Zwaraca tablice z wynikami zpytania
    return mysqli_fetch_array($row);
}//Wkladanie rekordow do tabeli

//***************************************2.WYSWIETLANIE PRODUTKOW W SKLEPIE************************************************************

function get_products(){ //Wyswieltanie listy produktow
    
   $query = query("SELECT * FROM products LIMIT 6");
   confirm($query);
   
   while($row = fetch_array($query)){
       
   $product_image = display_image($row['product_image']);
       
   $product = <<<DELIMETER
           
<div class="col-sm-4 col-lg-4 col-md-4">
    <div  class="thumbnail">
        <a href="item.php?id={$row['product_id']}"><img width='200px' src="../resources/{$product_image}" alt=""></a>
        <div class="caption">
            <h4 class="pull-right">PLN {$row['product_price']}</h4>
            <h4><a href="item.php?id={$row['product_id']}">{$row['product_title']}</a>
            </h4>
            <p>{$row['short_decs']}</p>
            <p>
              <a href="../resources/cart.php?add={$row['product_id']}" class="btn btn-primary btn-shop">Kup Teraz!</a> 
              <a href="item.php?id={$row['product_id']}" class="btn btn-default">Więcej info</a>
           </p>
        </div>

    </div>
</div>        
DELIMETER;
       
      echo $product; 
       
   }
    
 }//Wyswietlanie produktow na stronie glownej
 
function get_categories(){
      $query = query("SELECT * FROM categories");
      confirm($query);              
      while($row = fetch_array($query)) {
$categories_list = <<<DELIMETER
           
<a href='category.php?id={$row['cat_id']}' class='list-group-item'>{$row['cat_title']}</a>
        
DELIMETER;
                        
echo $categories_list;                        
                        
                        
                    }
 }//Pobranie z bazy danych kategorii
       
function product_features(){
$query = query("SELECT * FROM products WHERE product_category_id=" .escape_string($_GET['id']). " ");
    confirm($query);
    while($row = fetch_array($query)){
         $product_image = display_image($row['product_image']);
         $product_feature = <<<DELIMETER
                <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="../resources/{$product_image}" alt="">
                    <div class="caption">
                        <h3>{$row['product_title']}</h3>
                        <p>{$row['short_decs']}</p>
                        <p>
                            <a href="../resources/cart.php?add={$row['product_id']}" class="btn btn-primary">Kup teraz!</a> <a href="item.php?id={$row['product_id']}" class="btn btn-default">Więcej info</a>
                        </p>
                    </div>
                </div>
            </div>
DELIMETER;
        echo $product_feature;
    }
}//Wyswietlanie specyfikacji produktow    
    
function product_shop(){
$query = query("SELECT * FROM products");
    confirm($query);
    while($row = fetch_array($query)){
        $product_image = display_image($row['product_image']);
        $product_feature = <<<DELIMETER
                <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="../resources/{$product_image}" alt="">
                    <div class="caption">
                        <h3>{$row['product_title']}</h3>
                        <p>{$row['short_decs']}</p>
                        <p>
                            <a href="../resources/cart.php?add={$row['product_id']}" class="btn btn-primary">Kup Teraz!</a> 
                            <a href="item.php?id={$row['product_id']}" class="btn btn-default">Więcej info</a>
                        </p>
                    </div>
                </div>
            </div>
DELIMETER;
        echo $product_feature;
    }
}//Wyswietlanie produtkow w podstronie "sklep"

//***************************************3.PANEL ADMINISTRACYJNY************************************************************************

function login_user(){
    if(isset($_POST['submit'])){
        
  $username = escape_string($_POST['username']);
  $password = escape_string($_POST['password']);
  
  $query = query("SELECT * FROM users WHERE username= '{$username}' AND password= '{$password}'");
  confirm($query);      
        
        if(mysqli_num_rows($query) == 0){
            set_message("Bledny login lub haslo");
            redirect('login.php');
        }else{
            $_SESSION['username'] = $username;
            set_message('WITAMY W PANELU ADMINISTRACYJNYM');
            redirect('admin');
        }
        
    }
}//Logowanie do panelu administracyjnego

function send_message(){
    if(isset($_POST['submit'])){
        $to      = "tomasz.gesior@gmail.com";
        $name    = $_POST['name'];
        $subject = $_POST['subject'];
        $email   = $_POST['email'];
        $message = $_POST['message'];       
        $headers = "Od: {$from_name}, {$email}"; 
        $result  = mail($to, $subject, $message, $headers);
        
        if(!$result){
            set_message('Blad wysylania wiadomosci');
            redirect('contact.php');
        }else{
            set_message('Wiadomosc zostala wyslana');
        }
        
    }
    
}//Wysylanie wiadomosci na emaila administracyjny

//**************************************4.WYSWIETLANIE PRODUKTOW W PANELU ADMINISTRACYJNYM**********************************************

function display_orders(){
    
    $query = query("SELECT * FROM orders");
    confirm($query);
    
    while($row = fetch_array($query)){
        
        $orders = <<<DELIMETER
        <tr>
        <td>{$row['order_id']}</td>
        <td>{$row['order_amount']}</td>
        <td>{$row['order_transation']}</td>
        <td>{$row['order_currency']}</td>
        <td>{$row['order_status']}</td>
        <td><a class="btn btn-danger" href="../../resources/templates/back/delete_order.php?id={$row['order_id']}"><span class="glyphicon glyphicon-remove"></span></a></td>
        </tr>
    
    
DELIMETER;
                echo $orders;
    
}
}//Wyswietlenie zlozonych zamowien

function display_image($picture){
    return "uploads" . DS . $picture;
}//Wyswietlenie obrazka produktu

function get_products_in_admin(){
    
   $query = query("SELECT * FROM products");
   confirm($query);
   
   while($row = fetch_array($query)){
   $category      = show_product_category_title($row['product_category_id']);   
   $product_image = display_image($row['product_image']);     
   $product       = <<<DELIMETER
           
   <tr>
            <td>{$row['product_id']}</td>
            <td>{$row['product_title']} <br>
              <a href="index.php?edit_product&id={$row['product_id']}"><img width='100' src="../../resources/{$product_image}" alt=""></a>
            </td>
            <td>{$category}</td>
            <td>{$row['product_price']}</td>
            <td>{$row['product_quantity']}</td>
            <td><a class="btn btn-danger" href="../../resources/templates/back/delete_product.php?id={$row['product_id']}"><span class="glyphicon glyphicon-remove"></span></a></td>
        </tr>
DELIMETER;
       
      echo $product; 
       
   }
    
    
    
}//Wyswietlenie listy produtkow 

function show_product_category_title($product_category_id){
        $category_query = query("SELECT * FROM categories WHERE cat_id ='{$product_category_id}' ");
        confirm($category_query);
        
        while($category_row = fetch_array($category_query)){
            return $category_row['cat_title'];
        }
    }//Wyswietlenie nazy kategorii produktu

function add_product(){
    
    if(isset($_POST['publish'])){
        //Pole z formularza
        $product_title          = escape_string($_POST['product_title']);
        $product_category_id    = escape_string($_POST['product_category_id']);
        $product_price          = escape_string($_POST['product_price']);
        $product_description    = escape_string($_POST['product_description']);
        $short_decs             = escape_string($_POST['short_decs']);
        $product_quantity       = escape_string($_POST['product_quantity']);
        //Obsluga wgrywanych plikow
        $product_image          = escape_string($_FILES['file']['name']);
        $image_temp_location    = escape_string($_FILES['file']['tmp_name']);
        
        move_uploaded_file($image_temp_location, UPLOAD_DIRECTORY . DS . $product_image); //Przenoszenie plikow do uploads
        
        
        $query = query("INSERT INTO products( product_title , product_category_id, product_price, product_description, short_decs, product_quantity, product_image) VALUES ('{$product_title}' , '{$product_category_id}', '{$product_price}', '{$product_description}', '{$short_decs}', '{$product_quantity}', '{$product_image}')"); //Dodanie nowego produktu do bazy danych
        confirm($query);
        $last_id = last_id();
        set_message("Dodano nowy produkt o numerze id " . $last_id); //Ustanowienie wiadomosci
        redirect("index.php?products");
        
        
        
    }
    
    
    
}//Dodanie produktu do bazy danych

function show_categories_add_product(){
      $query = query("SELECT * FROM categories");
      confirm($query);              
      while($row = fetch_array($query)) {
$categories_options = <<<DELIMETER
           
<option value="{$row['cat_id']}">{$row['cat_title']}</option>
        
DELIMETER;
                        
echo $categories_options;                        
                        
                        
                    }
 }//Wyswietlenie kategorii przy dodawaniu produktu
 
function update_product(){
    
    if(isset($_POST['update'])){
        //Pole z formularza
        $product_title          = escape_string($_POST['product_title']);
        $product_category_id    = escape_string($_POST['product_category_id']);
        $product_price          = escape_string($_POST['product_price']);
        $product_description    = escape_string($_POST['product_description']);
        $short_decs             = escape_string($_POST['short_decs']);
        $product_quantity       = escape_string($_POST['product_quantity']);
        //Obsluga wgrywanych plikow
        $product_image          = escape_string($_FILES['file']['name']);
        $image_temp_location    = escape_string($_FILES['file']['tmp_name']);
        
        if(empty($product_image)){
            $get_pic = query("SELECT * FROM products WHERE product_id=". escape_string($_GET['id']) . " ");
            confirm($get_pic);
            
            while($pic = fetch_array($get_pic)){
                $product_image = $pic['product_image'];
            }
                    
        }
        
        move_uploaded_file($image_temp_location, UPLOAD_DIRECTORY . DS . $product_image); //Przenoszenie plikow do uploads
        
        $query  = "UPDATE products SET ";
        $query .= "product_title       = '{$product_title}'      , ";
        $query .= "product_category_id = '{$product_category_id}', ";
        $query .= "product_price       = '{$product_price}'      , ";
        $query .= "product_description = '{$product_description}', ";
        $query .= "short_decs          = '{$short_decs}'         , ";
        $query .= "product_quantity    = '{$product_quantity}'   , ";
        $query .= "product_image       = '{$product_image}'        ";
        $query .= "WHERE product_id = " . escape_string($_GET['id'] );
        
        
        
        
        
        $send_updated_query = query($query);
        confirm($send_updated_query);
        set_message("Zmieniono produkt"); //Ustanowienie wiadomosci
        redirect("index.php?products");
        
        
        
    }
    
    
    
}//Edycja produktow

function show_categories_in_admin(){
    
    $query = "SELECT * FROM categories";
    $category_query = query($query);
    confirm($category_query);
    
    while($row= fetch_array($category_query)){
        $cat_title = $row['cat_title'];
        $cat_id = $row['cat_id'];
        
     $category = <<<DELIMETER
        <tr>
            <td>{$cat_id}</td>
            <td>{$cat_title}</td>
            <td><a class="btn btn-danger" href="../../resources/templates/back/delete_category.php?id={$row['cat_id']}"><span class="glyphicon glyphicon-remove"></span></a></td>
        </tr>
   
   
   
   
DELIMETER;
    echo $category;
    }
    
    
}//Wyswietlenie kategorii 

function add_category(){
    if(isset($_POST['add_category'])){
        
        $cat_title = escape_string($_POST['cat_title']);
        
        if(empty($cat_title)){
            
            echo "<p class='bg-danger'>Nazwa kategorii nie moze byc pusta</p>";
        }else{
        
        $query = query("INSERT INTO categories (cat_title) VALUES ('{$cat_title}')");
        confirm($query);
        set_message('Kategoria zostala utworzona');
        
    }
}
}//Dodanie kategorii do bazy danych

function display_users(){
    
    $query = "SELECT * FROM users";
    $category_query = query($query);
    confirm($category_query);
    
    while($row= fetch_array($category_query)){
        $user_name = $row['username'];
        $user_id   = $row['user_id'];
        $email     = $row['email'];
        $password  = $row['passowrd'];
        
     $user = <<<DELIMETER
        <tr>
            <td>{$user_id}</td>
            <td>{$user_name}</td>
            <td>{$email}</td>
            <td><a class="btn btn-danger" href="../../resources/templates/back/delete_user.php?id={$row['user_id']}"><span class="glyphicon glyphicon-remove"></span></a></td>
        </tr>
   
   
   
   
DELIMETER;
    echo $user;
    }
    
    
}//Wyswietlenie listy uzytkwoinkow

function get_reports(){
    
   $query = query("SELECT * FROM reports");
   confirm($query);
   
   while($row = fetch_array($query)){
   
   
   
  
       
   $reports = <<<DELIMETER
           
         <tr>
            <td>{$row['report_id']}</td>
            <td>{$row['product_id']}</td>
            <td>{$row['order_id']}</td>
            <td>{$row['product_title']}</td>
            <td>{$row['product_price']} </td>
            <td>{$row['product_quantity']}</td>
            <td><a class="btn btn-danger" href="../../resources/templates/back/delete_reports.php?id={$row['report_id']}"><span class="glyphicon glyphicon-remove"></span></a></td>
        </tr>
DELIMETER;
       
      echo $reports; 
       
   }
    
    
    
}//Wyswietlenie raportow

//*******************************************5.OBSLUGA SLIDERA***********************************************************

function add_slides(){
    
    if(isset($_POST['add_slide'])){
        
        $slide_title     = escape_string($_POST['slide_title']);
        $slide_image     = escape_string($_FILES['file']['name']);
        $slide_image_loc = escape_string($_FILES['file']['tmp_name']);
        
        if(empty($slide_title) || empty($slide_image)){
            
            echo "<p class='bg-danger'>Slider musi zawierac tytul i obrazek</p>";
            
        }else{
            
            
         move_uploaded_file($slide_image_loc, UPLOAD_DIRECTORY . DS . $slide_image);   
            
         $query = query("INSERT INTO slides(slide_title, slide_image) VALUES ('{$slide_title}', '{$slide_image}')");
         confirm($query);
         set_message('Dodano nowy slider');
         redirect("index.php?slides");
        }
        
    }
    
    
}//Dodanie nowego slidera

function get_current_slide(){
    
    $query = query("SELECT * FROM slides ORDER BY slide_id DESC LIMIT 1");
    confirm($query);
    
    while($row = fetch_array($query)){
        
        $slide_active_admin = <<<DELIMETER
                
              <img class="img-responsive" src="../../resources/uploads/{$row['slide_image']}" alt="">              
DELIMETER;
        
     echo $slide_active_admin;   
        
    }
    
}//Pobranie aktualnie wyswietlanego slidera

function get_slides(){
    
    $query = query("SELECT * FROM slides");
    confirm($query);
    
    while($row = fetch_array($query)){
        
        $slides = <<<DELIMETER
                
            <div class="item ">                
              <img width='200px' class="slide-image" src="../resources/uploads/{$row['slide_image']}" alt="">             
            </div>    
                
DELIMETER;
        
     echo $slides;   
        
    }
    
}//Wyswietlenie sliderow na stronie glownej

function get_active(){
    
    $query = query("SELECT * FROM slides ORDER BY slide_id DESC LIMIT 1");
    confirm($query);
    
    while($row = fetch_array($query)){
        
        $slide_active = <<<DELIMETER
                
            <div class="item active">
              <img width='200px' class="slide-image" src="../resources/uploads/{$row['slide_image']}?text=haha" alt="">
            </div>    
                
DELIMETER;
        
     echo $slide_active;   
        
    }
    
}//Wyswietlenie aktywnego slidera na stronie glownej

function get_slide_thumbnails(){
    
    $query = query("SELECT * FROM slides ORDER BY slide_id ASC");
    confirm($query);
    
    while($row = fetch_array($query)){
        
        $slide_thumb_admin = <<<DELIMETER
                
            
    <div class="col-xs-6 col-md-3 image_container">
        <a href="index.php?delete_slide_id={$row['slide_id']}">
            <img width='250' class="img-responsive slide_image" src="../../resources/uploads/{$row['slide_image']}" alt="">
        </a>
        <div class="caption">
            <p>{$row['slide_title']}</p>
        </div>
    </div>
            
                
DELIMETER;
        
     echo $slide_thumb_admin;   
        
    }
    
    
}//Wyswietlenie miniaturek slidera