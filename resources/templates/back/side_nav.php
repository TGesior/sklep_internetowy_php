<div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav side-nav">
        <li class="active">
            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Panel główny/w budowie</a>
        </li>
        <li class="">
            <a href="index.php?orders"><i class="fa fa-fw fa-bar-chart-o"></i> Zamówienia</a>
        </li>
        <li class="">
            <a href="index.php?reports"><i class="fa fa-fw fa-bar-chart-o"></i> Raporty</a>
        </li>
        <li>
            <a href="index.php?products"><i class="fa fa-fw fa-table"></i> Zobacz produkty</a>
        </li>
        <li>
            <a href="index.php?add_product"><i class="fa fa-fw fa-table"></i> Dodaj produkt</a>
        </li>

        <li>
            <a href="index.php?categories"><i class="fa fa-fw fa-desktop"></i> Kategorie</a>
        </li>
        <li>
            <a href="index.php?users"><i class="fa fa-fw fa-wrench"></i>Użytkownicy</a>
        </li>

        <li>
            <a href="index.php?slides"><i class="fa fa-fw fa-wrench"></i>Slajdy</a>
        </li>
    </ul>
</div>