<div class="row">
    <h1 class="page-header">
        Raporty
    </h1>
    <h4 class="bg-success"><?php display_message(); ?></h4>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Id</th>
                <th>Produkt ID</th>
                <th>Zamówienie ID</th>
                <th>Cena</th>
                <th>Nazwa produktu</th>
                <th>Ilość</th>
            </tr>
        </thead>
        <tbody>
            <?php get_reports(); ?>
        </tbody>
    </table>